My very own bespoke, artisinal, hand-crafted tumblr theme
=========================================================

This here is a tumblr theme hacked together from:

* parts of my old textpattern blog,
    * which was hacked together from parts of my old wordpress blog,
        * which was hacked together from parts of an old php homegroan thing
            * which was really just a proof of concept for html 4.1 and css when that shit was new

With that sordid lineage, I've attempted to make this as modern and html5 as possible. It "meets my needs" and literally hasn't been browser-tested at all because isn't that the point of html5?

html5 themes with seventeen-jillion junky backwards-looking javascript hacks: just precious.
